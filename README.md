# Mockr

This project is to be used as a way to mock responses from a server. Don't want to setup a whole API server for testing out your front end application? This project is for you. Simply serve this code locally, put mock responses in the data folder, and you're good to go.

## Usage
Create files using hyphens as separators, where forward slashes would separate a typical request.
```
/this/application/should/1
data/this-application-should-1.json
```

Sending the `Content-type` header in your request will differntiate between response formats. For example:
```
url:
GET module/resources/verb/234

headers:
...
Content-type: text/csv
```
results in the following file (if it exists) being served:
```
data/module-resources-verb-234.csv
```
