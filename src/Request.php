<?php

namespace Mockr;

class Request
{
    protected $server = [];
    protected $headers = [];
    protected $contentTypeMap  = [
        'text/html'        => '.html',
        'application/json' => '.json',
        'text/csv'         => '.csv',
    ];

    public function __construct ($server = null, $headers = null)
    {
        $this->server = $server ?? $_SERVER;
        $this->headers = $headers ?? array_change_key_case(apache_request_headers());
        $this->init();
    }

    public function init()
    {
        array_walk($_SERVER, function(&$item, &$key) use (&$server) {
            $key = strtolower(strtr($key, [
                '_' => '-',
                'HTTP_' => '',
            ]));

            $this->server[$key] = $item;
        });
    }

    public function getUripath ($contentType = 'text/html')  : string
    {
        return strtolower(strtr($_GET['path'], [
            '/' => '-'
        ]));
    }

    public function getContentTypeMap () : array
    {
        return $this->contentTypeMap;
    }

    public function getContentType ($path, $headers = []) : string
    {
        if (array_key_exists('content-type', $headers)) {
            return $headers['content-type'];
        }
        return 'text/html';
    }

    public function getRequestHeaders() : array
    {
        return array_intersect_key($this->server, $this->headers);
    }

}
