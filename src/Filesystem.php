<?php

namespace Mockr;

class Filesystem
{
    public function getContentTypeMap () : array
    {
        return [
            'text/html'        => '.html',
            'application/json' => '.json',
            'text/csv'         => '.csv',
        ];
    }

    public function getFilepath ($path = '', $contentType = 'text/html') : string
    {
        $root = '../data/';
        $map = $this->getContentTypeMap();

        if ($this->isDirectPath($path)) {
            return realpath($root . $path);
        }

        $realpath = realpath($root . $path . $map[$contentType]);

        if (! $realpath) {
            throw new \RuntimeException('path not found : '. $root . $path . $map[$contentType]);
        }

        return $realpath;
    }

    public function read($path) : string
    {
        return file_get_contents($path);
    }

    public function isDirectPath ($path) : bool
    {
        $root = '../data/';
        $realpath = realpath($root . $path);
        if ($realpath) {
            return true;
        }
        return false;
    }

    public function getContentType ($path = '')
    {
        $map = $this->getContentTypeMap();

        if (! $this->isDirectPath($path)) {
            return false;
        }

        foreach ($map as $type => $ext) {
            if (strpos($path, $ext)) {
                return $type;
            }
        }
    }
}
