<?php

namespace Mockr;

class App
{
    protected $request;
    protected $filesystem;

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $this->request = new Request();
        $this->filesystem = new Filesystem();
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function getFilesystem()
    {
        return $this->filesystem;
    }

    public function run()
    {
        $request = $this->getRequest();
        $filesystem = $this->getFilesystem();

        $headers = $request->getRequestHeaders();
        $path = $request->getUripath();
        $contentType = $request->getContentType($path, $headers);
        if ($filesystem->getContentType($path)) {
            $contentType = $filesystem->getContentType($path);
        }
        $filepath = $filesystem->getFilepath($path, $contentType);
        $contents = $filesystem->read($filepath);

        header('Content-type: ' .  $contentType);
        echo $contents;
    }
}
